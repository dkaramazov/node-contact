var Airtable = require('airtable');
const airtable = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY }); 
const base = airtable.base(process.env.AIRTABLE_BASE_KEY);
module.exports.Templates = base('Templates');
module.exports.Contacts = base('Contacts');
module.exports.Users = base('Users');
module.exports.Replies = base('Replies');