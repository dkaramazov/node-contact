const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const methodOverride = require('method-override');
const helmet = require('helmet');

const contact = require('./routes/contacts');
const templates = require('./routes/templates');
const index = require('./routes/index');
const auth = require('./routes/auth');
const admin = require('./routes/admin');
const app = express();

const { truncate, stripTags, formatDate, select, editIcon } = require('./helpers/hbs');
require('./helpers/passport')(passport);

// helmet
// app.use(helmet());

// body-parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// method-override
app.use(methodOverride('_method'));

// handlebars
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({
    defaultLayout: 'main',
    helpers: {
        truncate: truncate,
        stripTags: stripTags,
        formatDate: formatDate,
        select: select,
        editIcon: editIcon
    },
    layoutsDir: path.join(__dirname, 'views', 'layouts'),
    partialsDir: path.join(__dirname, 'views', 'partials')
}));
app.set('view engine', 'handlebars');

// static
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: 'lottiemoon',
    store: new MongoStore({url: process.env.MONGO_URI}),
    resave: false,
    saveUninitialized: false
}));

// Setup passport
app.use(passport.initialize());
app.use(passport.session());

// Set global variables
app.use((req, res, next) => {
    res.locals.user = req.user || null;
    next();
})


// AUTH
app.use('/contacts', contact);
app.use('/templates', templates);
app.use('/auth', auth);
app.use('/admin', admin);
app.use('/', index);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;