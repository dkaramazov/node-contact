const express = require('express');
const router = express.Router();
const { checkAuthenticated, checkGuest } = require('../helpers/auth');
const { mapFromTableContact } = require('../helpers/models');
const { Contacts } = require('../api/airtable');

router.get('/', checkGuest, (req, res) => {
    res.render('index/welcome');
});

router.get('/dashboard', checkAuthenticated, (req, res) => {    
    var contacts = [];
    Contacts.select({
        view: 'New',
        sort: [{field: "Replied", direction: "asc"}]        
    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record){
            contacts.push(mapFromTableContact(record._rawJson.fields, record._rawJson.id));
        });        
        fetchNextPage();              
    }, function done(err){
        if (err) { console.error(err); return; }
        res.render('index/dashboard', { contacts });  
    });    
});

router.get('/about', (req, res) => {
    res.render('index/about');
});

module.exports = router;