const express = require('express');
const router = express.Router();
const { sendEmail } = require('../controllers/emailController');
const { sendText } = require('../controllers/textingController');
const { Contacts, Templates, Replies } = require('../api/airtable');
const { checkAuthenticated } = require('../helpers/auth');
const { createTableContact, mapFromTableContact, mapFromTableTemplate, mapFromTableReplies } = require('../helpers/models');

router.get('/', checkAuthenticated, (req, res) => {
    var contacts = [];
    Contacts.select({
        view: 'Finished'
    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            contacts.push(mapFromTableContact(record._rawJson.fields, record._rawJson.id));
        });
        fetchNextPage();
    }, function done(err) {
        if (err) { console.error(err); return; }
        res.render('index/dashboard', { contacts, message: 'Historical Data' });
    });
});

router.get('/add', (req, res) => {
    // res.setHeader("X-FRAME-OPTIONS", "ALLOW-FROM https://morganscottfilms.com/contact/");
    // res.setHeader("Content-Security-Policy", "frame-ancestors https://morganscottfilms.com/contact/");
    res.render('contacts/add', { layout: 'add' });
});

router.get('/reply/:contactId/:templateId', checkAuthenticated, (req, res) => {
    Templates.find(req.params.templateId, function (err, record) {
        if (err) {
            console.error(err);
            return res.redirect('/');
        }
        const template = mapFromTableTemplate(record._rawJson.fields, record._rawJson.id)
        Contacts.find(req.params.contactId, function (err, record) {
            if (err) {
                console.error(err);
                return res.redirect('/');
            }
            const contact = mapFromTableContact(record._rawJson.fields, record._rawJson.id);
            res.render('contacts/reply', { contact, template });
        });
    });
});

router.post('/reply/:id', checkAuthenticated, (req, res) => {
    Contacts.find(req.params.id, function (err, record) {
        if (err) { console.error(err); return; }
        const contact = mapFromTableContact(record._rawJson.fields, record._rawJson.id);
        var mailOptions = {
            from: req.user.email,
            to: contact.email,
            template: 'reply',
            subject: req.body.subject,
            body: req.body.body
        }
        sendEmail(mailOptions)
            .then(function (response) {
                if (process.env.NODE_ENV !== 'testing') {
                    Contacts.update(req.params.id, {
                        "Replied": true
                    }, function (err, record) {
                        if (err) { console.error(err); return; }
                        Replies.create({
                            "Body": req.body.body,
                            "Contact": [req.params.id]
                        }, function (err, record) {
                            if (err) { console.error(err); return; }
                            res.redirect('/dashboard');
                        });
                    });
                } else {
                    res.redirect('/dashboard');
                }
            })
            .catch(function (error) {
                if (error) {
                    console.error(error);
                    return res.redirect('/');
                }
            });
    });
});

router.get('/:id', checkAuthenticated, (req, res) => {
    Contacts.find(req.params.id, function (err, record) {
        if (err) {
            console.error(err);
            return res.redirect('/');
        }
        const contact = mapFromTableContact(record._rawJson.fields, record._rawJson.id);
        if (contact.replies && contact.replies.length > 0) {
            Replies.find(contact.replies[0], function (err, record) {
                if (err) {
                    console.error(err);
                    return res.redirect('/');
                }
                const reply = mapFromTableReplies(record._rawJson.fields, record._rawJson.id);
                res.render('contacts/show', { contact, reply })
            });
        } else {
            res.render('contacts/show', { contact });
        }
    });
});

router.put('/:id', checkAuthenticated, (req, res) => {
    Contacts.update(req.params.id, {
        "Replied": true        
    }, function (err, record) {
        if (err) { console.error(err); return; }
        res.redirect('/dashboard');
    });
});

router.post('/add', (req, res) => {
    Contacts.create(createTableContact(req.body), function (err, record) {
        if (err) { console.error(err); return; }
        const contact = mapFromTableContact(record._rawJson.fields, record._rawJson.id);
        const subject = `New inquiry from ${contact.name}`;
        const body = `
        <h3>${contact.name} + ${contact.fiance}</h3>
        <p>Date: ${contact.date}</p>
        <p>Email: ${contact.email}</p>
        <p>Venue: ${contact.venue}</p>
        <p>Coordinator/Planner: ${contact.coordinator}</p>
        <p>Budget: ${contact.budget}</p>
        <p>Quality: ${contact.quality}</p>
        <p>Details: ${contact.details}</p>
        <a href="${process.env.DEPLOYMENT_URL}/contacts/${record._rawJson.id}">REPLY TO ${contact.name}</a>
        `;
        var mailOptions = {
            from: process.env.EMAIL_USERNAME,
            to: process.env.EMAIL_USERNAME,
            template: 'notification',
            subject: subject,
            body: body
        }
        sendText(`You have a new inquiry from ${req.body.name}. Go to ${process.env.DEPLOYMENT_URL}/contacts/${record._rawJson.id}`)
            .then(function (response) {
                return sendEmail(mailOptions);
            })
            .then(function (response) {
                res.send('<h3 style="text-align:center;">Thanks! We\'ll contact you soon!</h3>');
            })
            .catch(function (error) {
                if (error) {
                    res.send('<h3 style="text-align:center;">An error occured.</h3>');
                }
            });
    });
});
module.exports = router;