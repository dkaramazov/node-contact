const express = require('express');
const router = express.Router();
const { Templates } = require('../api/airtable');
const { mapFromTableTemplate } = require('../helpers/models');
const { checkAuthenticated } = require('../helpers/auth');

router.get('/', checkAuthenticated, (req, res) => {
    Templates.select({}).firstPage(function (err, records) {
        if (err) return res.send('An error occurred.');
        records = records.map(record => {
            return mapFromTableTemplate(record._rawJson.fields, record._rawJson.id);
        });
        res.render('templates/index', { templates: records });
    });
});

router.get('/choose/:id', checkAuthenticated, (req, res) => {
    Templates.select({}).firstPage(function (err, records) {
        if (err) return res.send('An error occurred.');
        records = records.map(record => {            
            record = mapFromTableTemplate(record._rawJson.fields, record._rawJson.id);
            record.contactId = req.params.id;
            return record;
        });
        res.render('templates/choose', { templates: records });
    });
});

router.get('/my', checkAuthenticated, (req, res) => {
    Templates.select({
        maxRecords: 100
    }).firstPage(function (err, records) {
        if (err) return res.send('An error occurred.');
        records = records.map(record => {
            return mapFromTableTemplate(record._rawJson.fields, record._rawJson.id);
        }).filter(record => {
            return record.user && record.user.length > 0 ? record.user.indexOf(req.user.id) > -1 : false;
        });
        res.render('templates/index', { templates: records });
    });
});

router.get('/user/:id', checkAuthenticated, (req, res) => {
    Templates.select({
        maxRecords: 100        
    }).firstPage(function (err, records) {
        if (err) return res.send('An error occurred.');
        records = records.map(record => {
            return mapFromTableTemplate(record._rawJson.fields, record._rawJson.id);
        }).filter(record => {
            return record.user && record.user.length > 0 ? record.user.indexOf(req.params.id) > -1 : false;
        });
        res.render('templates/index', { templates: records });
    });
});

router.get('/edit/:id', checkAuthenticated, (req, res) => {
    Templates.find(req.params.id, function (err, record) {
        if (err) {
            console.error(err);
            return res.redirect('/');
        }
        res.render('templates/edit', { template: mapFromTableTemplate(record._rawJson.fields, record._rawJson.id) });
    });
});

router.put('/:id', (req, res) => {
    Templates.replace(req.params.id, {
        "Title": req.body.title,
        "Subject": req.body.subject,
        "Body": req.body.body,
        "User": [req.user.id]
    }, function (err, record) {
        if (err) { console.error(err); return; }
        res.redirect('/templates');
    });
});

router.delete('/:id', (req, res) => {
    Templates.destroy(req.params.id, function (err, deletedRecord) {
        if (err) { console.error(err); return; }
        res.redirect('/templates');
    });
});

router.get('/add', checkAuthenticated, (req, res) => {
    res.render('templates/add');
});

router.post('/', checkAuthenticated, (req, res) => {
    Templates.create({
        "Title": req.body.title,
        "Subject": req.body.subject,
        "Body": req.body.body,
        "User": [req.user.id]
    }, function (err, record) {
        if (err) { console.error(err); return; }
        res.redirect('/templates');
    });
});

router.get('/show/:id', (req, res) => {
    Templates.find(req.params.id, function (err, record) {
        if (err) {
            console.error(err);
            return res.redirect('/');
        }

        res.render('templates/show', { template: mapFromTableTemplate(record._rawJson.fields, record._rawJson.id) });
    });
});

module.exports = router;