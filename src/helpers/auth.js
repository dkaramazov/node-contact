module.exports = {
    checkAuthenticated: function(req, res, next){
        if(req.isAuthenticated() && req.user.allowAccess){
            return next();
        }
        if(req.user){
            res.render('index/register', {message: `Thank you, ${req.user.firstName} ${req.user.lastName}. We will review your request.`});
        } else{
            req.session.redirectUrl = req.originalUrl;
            res.redirect('/');
        }
    },
    checkAdmin: function(req, res, next){
        if(req.isAuthenticated() && req.user.isAdmin){
            return next();
        } 
        res.redirect('/');
    },
    checkGuest: function(req, res, next){
        if(req.isAuthenticated()){                                
            res.redirect('/dashboard');
        } else {
            return next();
        }
    }
}