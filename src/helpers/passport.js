const GoogleStrategy = require('passport-google-oauth20').Strategy;
const { } = require('../controllers/userController');
const { Users } = require('../api/airtable');
const { mapFromTableUser, createTableUser } = require('./models');

module.exports = function (passport) {
    passport.use(
        new GoogleStrategy({
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
            callbackURL: '/auth/google/callback',
            proxy: true
        }, (accessToken, refreshToken, profile, done) => {
            const image = profile.photos[0].value.substring(0, profile.photos[0].value.indexOf('?'));
            const newUser = {
                googleID: profile.id,
                firstName: profile.name.givenName,
                lastName: profile.name.familyName,
                email: profile.emails[0].value,
                image: image
            };
            // process.env.GOOGLE_REFRESH_TOKEN = refreshToken;
            // process.env.GOOGLE_ACCESS_TOKEN = accessToken;
            // process.env.EMAIL_USERNAME = profile.emails[0].value;
            Users.select({}).firstPage(function (err, records) {
                if (err) { console.error(err); return; }
                const record = records.find(record => {
                    return record.fields.GoogleID === profile.id;
                });
                if (record && record.fields) {
                    done(null, mapFromTableUser(record.fields, record.id));
                } else {
                    Users.create(createTableUser(newUser), function (err, record) {
                        if (err) { console.error(err); return; }
                        done(null, mapFromTableUser(record.fields, record.id));
                    });
                }
            });
        })
    );

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        Users.find(id, function (err, record) {
            if (err) { console.error(err); return; }
            done(null, mapFromTableUser(record._rawJson.fields, record._rawJson.id));
        });
    });
}