module.exports.createTableUser = (source) => {
    return {
        "GoogleID": source.googleID,
        "Last Name": source.lastName,
        "First Name": source.firstName,
        "Email": source.email,
        "Image": source.image,
        "allowAccess": source.allowAccess || false,
        "isAdmin": source.isAdmin || false,
        "Templates": source.templates || [],
        "mobile_number": source.mobile || []
    }
}

module.exports.mapFromTableUser = (source, id) => {
    return {
        id,
        googleID: source['GoogleID'],
        firstName: source['First Name'],
        lastName: source['Last Name'],
        email: source['Email'],
        image: source['Image'],
        allowAccess: source['allowAccess'] || false,
        isAdmin: source['isAdmin'] || false,
        templates: source['Templates'] || [],
        mobile: source['mobile_number'] || []
    }
}

module.exports.createTableTemplate = (source) => {
    return {
        "Title": source.title,
        "Subject": source.subject,
        "Body": source.body,
        "Created Date": source.create_date,
        "User": source.user
    }
}

module.exports.mapFromTableTemplate = (source, id) => {
    return {
        id,
        title: source['Title'],
        subject: source['Subject'],
        body: source['Body'],
        create_date: source['Created Date'],
        user: source['User']
    }
}

module.exports.createTableContact = (source) => {
    return {
        "Contact Form": source.form || 'Not specified',
        "Name": source.name,
        "Your Fiancé's Name:": source.fiance,
        "Email Address:": source.email,
        "Where did you hear about us?": source.source,
        "Referral": source.referral,
        "Wedding Date:": source.date,
        "Wedding Venue/Location:": source.location,
        "Venue Name": source.venue,
        "Coordinator": source.coordinator,
        "Wedding Video Budget:": source.budget,
        "Tell us any other details about your wedding that may help us!": source.details,
        "Quality": source.quality,
        "Referral": source.referral,
        "Replied": source.replied,
        "Replies": source.replies
    }
}

module.exports.mapFromTableContact = (source, id) => {
    return {
        id,
        form: source["Contact Form"],
        name: source["Name"],
        fiance: source["Your Fiancé's Name:"],
        email: source["Email Address:"],
        source: source["Where did you hear about us?"],
        referral: source["Referral"],
        date: source["Wedding Date:"],
        location: source["Wedding Venue/Location:"],
        venue: source["Venue Name"],
        coordinator: source["Coordinator"],
        budget: source["Wedding Video Budget:"],
        details: source["Tell us any other details about your wedding that may help us!"],
        quality: source["Quality"],
        replied: source["Replied"],
        replies: source["Replies"]
    }
}
module.exports.mapFromTableReplies = (source, id) => {
    return {
        id,
        body: source["Body"],
        date: source["Date"],
        template: source["Template"],
        contact: source["Contact"]
    }
}
module.exports.createTableReply = (source) => {
    return {
        "Body": source.body,
        "Template": source.template,
        "Date": source.date,
        "Contact": source.contact
    }
}