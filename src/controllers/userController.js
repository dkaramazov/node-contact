const { Users } = require('../api/airtable');
const { mapFromTableUser } = require('../helpers/models');

exports.getAll = (req, res) => {
    Users.select({}).firstPage(function (err, records) {
        if (err) return res.send('An error occurred.');
        records = records.map(record => {
            return mapFromTableUser(record._rawJson.fields, record._rawJson.id);
        });
        res.render('admin/users', { users: records });
    });
};

exports.getMobile =  (req, res) => {
    Users.find(req.params.id, function (err, record) {
        if (err) { console.error(err); return; }
        const user = mapFromTableUser(record._rawJson.fields, record._rawJson.id);
        res.render('admin/mobile', { id: user.id, mobile: user.mobile});       
    });
};

exports.upsertMobile = (req, res) => {
    Users.find(req.params.id, function (err, record) {
        if (err) { console.error(err); return; }
        Users.update(req.params.id, {
            "mobile_number": req.body.mobile
        }, function (err, record) {
            if (err) { console.error(err); return; }
            res.redirect('/admin');
        });
    });
};

exports.createUser = (req, res) => {
    Users.create({
        "GoogleID": req.body.googleID,
        "First Name": req.body.firstName,
        "Last Name": req.body.lastName,
        "Email": req.body.email,
        "Image": reqb.body.image,
        "isAdmin": req.body.isAdmin,
        "allowAccess": req.body.allowAccess,
        "Templates": req.body.templates
    }, function (err, record) {
        if (err) { console.error(err); return; }
        res.send(record._rawJson);
    });
}

exports.toggleAccess = (id, cb) => {
    Users.find(id, function (err, record) {
        if (err) { console.error(err); return; }
        Users.update(id, {
            "allowAccess": !record._rawJson.fields.allowAccess
        }, function (err, record) {
            if (err) { console.error(err); return; }
            cb(mapFromTableUser(record._rawJson.fields, record._rawJson.id));
        });
    });
}

exports.toggleAdmin = (id, cb) => {
    Users.find(id, function (err, record) {
        if (err) { console.error(err); return; }
        Users.update(id, {
            "isAdmin": !record._rawJson.fields.isAdmin
        }, function (err, record) {
            if (err) { console.error(err); return; }
            cb(mapFromTableUser(record._rawJson.fields, record._rawJson.id));
        });
    });
}
