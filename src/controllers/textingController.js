const plivo = require('plivo');
const client = new plivo.Client(process.env.PLIVO_AUTH_ID, process.env.PLIVO_AUTH_TOKEN);

module.exports.sendText = async (message) => {
    try {
        const response = await client.messages.create(process.env.SOURCE_NUMBER, process.env.DESTINATION_NUMBER, message);
        return response;
    } catch (error) {
        return error;
    }
}
